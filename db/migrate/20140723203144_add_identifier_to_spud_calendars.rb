class AddIdentifierToSpudCalendars < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_calendars, :identifier, :string
    add_index :spud_calendars, :identifier
  end
end
