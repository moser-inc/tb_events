class RemoveColorFromSpudCalendars < ActiveRecord::Migration[4.2]
  def up
    remove_column :spud_calendars, :color
  end

  def down
    add_column :spud_calendars, :color, 'binary(6)'
  end
end
