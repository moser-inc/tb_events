# Change Log

This log will detail all major releases of TB Events. Minor patch updates may be omitted if they are simple bug fixes. 

## 1.1.1

- Replace overcomplicated calendar builder module with a much simplified version
- Updates for Rails 4.1
- Add `Spud::SpudCalendarEventModel` model class for apps to extend
- Misc fixes

## 1.1.0

- Now requires Rails 4 and TB Core 1.2

## v1.0.2

- Added support for events that span multiple days in the `build_calendar_for` helper.
