Rails.application.routes.draw do
  # Calendar
  get 'calendar(/:month/:year)' => 'calendars#show', :as => :calendar
  resources :calendar_events, only: [:index, :show], path: '/events'

  namespace :admin do
    resources :calendar_events, except: [:index]
    resources :calendars, except: [:index]
    get 'events(/:month(/:year(/:calendar)))' => 'calendar_events#index', :as => 'list_spud_calendar_events'
  end
end
