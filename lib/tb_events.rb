module Spud
  module Events
    require 'spud_events/configuration'
    require 'spud_events/engine' if defined?(Rails)
  end
end
