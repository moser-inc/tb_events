require 'tb_core'

module SpudEvents
  class Engine < Rails::Engine
    require "#{root}/lib/spud_events/calendar_builder"

    engine_name :tb_events
    config.autoload_paths << "#{root}/lib"

    initializer :admin do
      TbCore.configure do |config|
        config.admin_applications += [{ name: 'Events', thumbnail: 'spud/admin/events_thumb.png', url: '/admin/events', order: 10 }]
      end
    end

    initializer :assets do |_config|
      TbCore.append_admin_stylesheets('admin/events/application')
      TbCore.append_admin_javascripts('admin/events/application')
    end

    initializer 'tb_events.assets' do
      Rails.application.config.assets.precompile += ['spud/admin/events_thumb.png']
    end
  end
end
