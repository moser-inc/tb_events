module SpudEvents
  include ActiveSupport::Configurable
  config_accessor :content_for, :calendar_layout
  self.content_for = nil
  self.calendar_layout = nil
end
