class SpudEvents::CalendarBuilder
  DEFAULT_HEADERS = %w(Sunday Monday Tuesday Wednesday Thursday Friday Saturday)
  START_DAY = :sunday

  delegate :content_tag, to: :view
  attr_accessor :view, :date, :callback

  def initialize(view, date, opts)
    @view = view
    @date = date
    @headers = opts.delete(:headers) || DEFAULT_HEADERS
  end

  def table(&block)
    @callback = block
    content_tag :table, class: 'calendar' do
      header + week_rows
    end
  end

  private

  def header
    content_tag :tr do
      @headers.map { |day| content_tag :th, day, class: 'day-header' }.join.html_safe
    end
  end

  def week_rows
    weeks.map do |week|
      content_tag :tr do
        week.map { |day| day_cell(day) }.join.html_safe
      end
    end.join.html_safe
  end

  def day_cell(day)
    content_tag :td, view.capture(day, &callback), :class => day_classes(day), 'data-date' => day.strftime('%F')
  end

  def day_classes(day)
    classes = ['day-cell']
    classes << 'today' if day == Date.today
    classes << 'not-month' if day.month != date.month
    classes.empty? ? nil : classes.join(' ')
  end

  def weeks
    first = date.beginning_of_month.beginning_of_week(START_DAY)
    last = date.end_of_month.end_of_week(START_DAY)
    (first..last).to_a.in_groups_of(7)
  end
end
