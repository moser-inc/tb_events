class Admin::CalendarsController < Admin::ApplicationController
  add_breadcrumb 'Events', :admin_list_spud_calendar_events_path

  belongs_to_app :events

  respond_to :html, :xml, :json, :js

  def new
    @page_name = 'New Calendar'
    @calendar = SpudCalendar.new
    respond_with(@calendar)
  end

  def create
    @calendar = SpudCalendar.new(calendar_params)
    if @calendar.save
      redirect_to admin_list_spud_calendar_events_path
    else
      render action: 'new'
    end
  end

  def edit
    @page_name = 'Edit Calendar'
    @calendar = SpudCalendar.find(params[:id])
    respond_with(@calendar)
  end

  def update
    @calendar = SpudCalendar.find(params[:id])
    if @calendar.update_attributes(calendar_params)
      flash[:notice] = 'Calendar was successfully updated.'
      redirect_to admin_list_spud_calendar_events_path
    else
      render action: 'edit'
    end
  end

  def destroy
    @calendar = SpudCalendar.find(params[:id])
    @calendar.spud_calendar_events.each do |event|
      event.update_attribute(:spud_calendar_id, 0)
    end
    @calendar.destroy
    respond_with(@calendar) do |format|
      format.js { render(nothing: true) }
    end
  end

  private

  def calendar_params
    params.require(:spud_calendar).permit(:title)
  end
end
