class CalendarEventsController < ApplicationController
  respond_to :html, :json
  layout SpudEvents.config.calendar_layout

  def index
    @events = SpudCalendarEvent.ordered.paginate(per_page: 10, page: params[:page])
    respond_with @events
  end

  def show
    @event = SpudCalendarEvent.find(params[:id])
    respond_with @event
  end
end
