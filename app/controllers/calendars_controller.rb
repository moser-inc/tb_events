class CalendarsController < ApplicationController
  respond_to :html, :json
  layout SpudEvents.config.calendar_layout
  before_action :calendar_date

  def show
    if params[:calendar] && calendar = SpudCalendar.find_by_title(params[:calendar].titleize)
      @events = calendar.spud_calendar_events.in_month_of(@calendar_date).order(:start_at).to_a
    else
      @events = SpudCalendarEvent.in_month_of(@calendar_date).order(:start_at).to_a
    end
    @current_calendar = params[:calendar] ? params[:calendar] : 0
    respond_with @events
  end

  private

  def calendar_date
    if params[:month]
      year = params[:year] ? params[:year].to_i : Date.today.year
      begin
        @calendar_date = Date.new(year, params[:month].to_i)
      rescue
        @calendar_date = Date.today
      end
    else
      @calendar_date = Date.today
    end
  end
end
