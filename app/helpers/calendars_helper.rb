module CalendarsHelper
  # Builds a calendar for the given date
  #
  # * date: Determines the month to be displayed. Given date will have the "today" css class
  # * opts:
  #   - headers: Array of weekday headers (optional)
  #
  def spud_calendar_for_date(date, opts = {}, &block)
    events = SpudCalendarEvent.in_month_of(date)
    spud_calendar_for_events(events, date, opts, &block)
  end

  # Builds a calendar for the given date and identifier
  #
  # * identifier: Symbol for the SpudCalendar you wish to display
  # * date: Determines the month to be displayed. Given date will have the "today" css class
  # * opts:
  #   - headers: Array of weekday headers (optional)
  #
  def spud_calendar_for_identifier(identifier, date, opts = {}, &block)
    calendar = SpudCalendar.with_identifier(identifier)
    if calendar.present?
      events = calendar.spud_calendar_events.in_month_of(date)
    else
      logger.warn "Failed to load calendar by identifer '#{identifier}'. Does it exist?"
      events = []
    end
    spud_calendar_for_events(events, date, opts, &block)
  end

  # Builds a calendar for the given date, with events
  #
  # * events: An array or relation of SpudCalendarEvent records
  # * date: Determines the month to be displayed. Given date will have the "today" css class
  # * opts:
  #   - headers: Array of weekday headers (optional)
  #
  def spud_calendar_for_events(events, date, opts = {}, &block)
    events_array = events.to_a
    cache(['spud-calendar', date.to_s, events]) do
      concat(
        SpudEvents::CalendarBuilder.new(self, date, opts).table do |day|
          events = extract_events_for_date(events_array, day)
          if block
            block.call(day, events)
          else
            concat content_tag(:span, day.day.to_s, class: 'calendar-day')
            if events.length > 0
              list_items = events.collect do |event|
                '<li>' + link_to(event.title, calendar_event_path(event)) + '</li>'
              end.join('')
              concat content_tag(:ul, raw(list_items), class: 'events-list')
            end
          end
        end
      )
    end
  end

  def link_to_previous_calendar_month(date)
    date = date.advance(months: -1)
    calendar = params[:calendar] ? params[:calendar] : nil
    link_to('&laquo; Previous Month'.html_safe, calendar_path(month: date.month, year: date.year, calendar: calendar), class: 'spud_events_calendar_link previous_calendar btn').html_safe
  end

  def link_to_next_calendar_month(date)
    date = date.advance(months: 1)
    calendar = params[:calendar] ? params[:calendar] : nil
    link_to('Next Month &raquo;'.html_safe, calendar_path(month: date.month, year: date.year, calendar: calendar), class: 'spud_events_calendar_link next_calendar btn').html_safe
  end

  private

  def extract_events_for_date(events, date)
    events.select do |event|
      date >= event.start_date && date <= event.end_date
    end
  end
end
