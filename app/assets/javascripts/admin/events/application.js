//= require_self
//= require_directory

(function(){

spud.admin.events = {
  init: function(){
    $(document).on('ajax:success','.js-spud-calendar-delete-action', deletedCalendar);
  }
};

var deletedCalendar = function(){
  $(this).closest('.spud_events_admin_calendar_legend_item').fadeOut(500, function(){ $(this).remove(); });
};

})();
