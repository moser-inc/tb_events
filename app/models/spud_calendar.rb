class SpudCalendar < ActiveRecord::Base
  has_many :spud_calendar_events, dependent: :destroy
  validates_presence_of :title
  validates :identifier, presence: true, uniqueness: true

  before_validation :set_identifier

  def self.with_identifier(identifier)
    where(identifier: identifier).first
  end

  def color
    modulo = (id - 1) % COLORS.length
    COLORS[modulo]
  end

  COLORS = %w(
    4D854B
    f89406
    b94a48
    3a87ad
    999999
    333333)

  private

  def set_identifier
    if identifier.nil? && title.present?
      self.identifier = title.parameterize.underscore
    end
  end
end
