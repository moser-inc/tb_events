class SpudEvents::CalendarEventModel < ActiveRecord::Base
  self.table_name = 'spud_calendar_events'

  belongs_to :spud_calendar
  validates_presence_of :title, :start_at, :end_at, :spud_calendar_id

  scope :ordered, -> { order('start_at desc') }
  scope :in_month_of, lambda { |date|
    where('start_at BETWEEN ? AND ? OR end_at BETWEEN ? AND ?', date.beginning_of_month, date.end_of_month, date.beginning_of_month, date.end_of_month).order('start_at asc')
    # where(:start_at => date.beginning_of_month...date.end_of_month)
  }
  scope :upcoming, ->(limit = false) { where('start_at >= ?', Date.today).order('start_at asc').limit(limit) }

  def start_date
    start_at.to_date
  end

  def end_date
    end_at.to_date
  end

  def days_span
    (end_at.to_date - start_at.to_date).to_i + 1
  end

  def falls_on?(day)
    start_date = start_at.beginning_of_day.to_date
    end_date = end_at.end_of_day.to_date
    day >= start_date && day <= end_date
  end
end
